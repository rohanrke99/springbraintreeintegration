package com.rohan.braintree.spring;

import com.braintreegateway.BraintreeGateway;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

import java.io.File;
import java.io.StringReader;

@SpringBootApplication
@EnableAutoConfiguration(exclude = {
        SecurityAutoConfiguration.class
})
public class BraintreeSpringIntegrationApplication {

    public static BraintreeGateway gateway;
    private static final String DEFAULT_CONFIG_FILENAME = "config.properties";

    public static void main(String[] args) {

        loadBraintreeGateway();
        SpringApplication.run(BraintreeSpringIntegrationApplication.class, args);
    }


    private static void loadBraintreeGateway(){
        File configFile = new File(DEFAULT_CONFIG_FILENAME);
        try {
            if(configFile.exists() && !configFile.isDirectory()) {
                gateway = BraintreeGatewayFactory.fromConfigFile(configFile);
            } else {
                gateway = BraintreeGatewayFactory.fromConfigMapping(System.getenv());
            }
        } catch (NullPointerException e) {
            System.err.println("Could not load Braintree configuration from config file or system environment.");
            System.exit(1);
        }

    }
}
