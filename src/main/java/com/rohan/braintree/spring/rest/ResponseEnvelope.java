package com.rohan.braintree.spring.rest;

import lombok.Data;
import org.springframework.http.HttpStatus;

import java.util.List;

@Data
public class ResponseEnvelope<T> {
    private String message;
    private HttpStatus status;
    private T data;
    private List<String> error;


    public ResponseEnvelope (HttpStatus status, String message, T data){
        this.message = message;
        this.status = status;
        this.data = data;
    }

    public ResponseEnvelope (HttpStatus status, String message, List<String> error){
        this.message = message;
        this.status = status;
        this.error = error;
    }

    public ResponseEnvelope (HttpStatus status, String message){
        this.message = message;
        this.status = status;
    }

}
