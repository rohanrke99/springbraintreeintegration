package com.rohan.braintree.spring.repo;

import com.rohan.braintree.spring.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface UserRepository extends MongoRepository<User,String> {

    User findUserById(String userId);

    User findUserByEmail(String emailId);


    @Override
    @Transactional(timeout = 10)
    List<User> findAll();


}
