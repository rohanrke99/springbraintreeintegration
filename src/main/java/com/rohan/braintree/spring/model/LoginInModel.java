package com.rohan.braintree.spring.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class LoginInModel {

    private String email;
    private String password;
}
