package com.rohan.braintree.spring.controller;

import com.rohan.braintree.spring.entity.User;
import com.rohan.braintree.spring.model.LoginInModel;
import com.rohan.braintree.spring.model.UserModel;
import com.rohan.braintree.spring.rest.ResourceNotFoundException;
import com.rohan.braintree.spring.rest.ResponseEnvelope;
import com.rohan.braintree.spring.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@RestController
public class UserController {

    private final Logger logger = Logger.getLogger(UserController.class.getName());

    @Autowired
    private UserService userService;



    @PostMapping(value = "/api/user/register")
    public @ResponseBody
    ResponseEnvelope<User> registerUser(@Valid @RequestBody UserModel user){


        logger.log(Level.INFO,"creating new user");
        User newCollege = userService.saveUser(user);
        if (newCollege== null){
            return new ResponseEnvelope<>(HttpStatus.OK,"unable to register user",newCollege);
        }
        return new ResponseEnvelope<>(HttpStatus.CREATED,"Successfully registered user",newCollege);
    }


   @PostMapping(value = "/api/user/login")
    public @ResponseBody
    ResponseEnvelope<User> signInUser(@Valid @RequestBody LoginInModel model){


        logger.log(Level.INFO,"signing in user with email " + model.getEmail());
        try {
            User user  = userService.loginIn(model);
            return new ResponseEnvelope<>(HttpStatus.CREATED,"Logged In user",user);

        }catch (IllegalAccessException e){
            e.printStackTrace();
            return new ResponseEnvelope<>(HttpStatus.UNAUTHORIZED,e.getMessage());
        }catch (ResourceNotFoundException e){
            e.printStackTrace();
            return new ResponseEnvelope<>(HttpStatus.NOT_FOUND,e.getMessage());
        }

    }



    @GetMapping(value = "/api/user/list")
    public @ResponseBody
    ResponseEnvelope<List<User>> getUsers(){

        logger.log(Level.SEVERE,"current thread : " + Thread.currentThread().getName());

        logger.log(Level.INFO,"searching all user");

        List<User> list  = userService.getAllUsers();


        if (list== null){
            return new ResponseEnvelope<>(HttpStatus.OK,"unable to register user",list);
        }
        return new ResponseEnvelope<>(HttpStatus.FOUND,"Successfully registered user",list);
    }


}
