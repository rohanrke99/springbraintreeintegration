package com.rohan.braintree.spring.controller;

import com.rohan.braintree.spring.rest.ResponseEnvelope;
import com.rohan.braintree.spring.service.PaymentService;
import com.rohan.braintree.spring.service.UserService;
import com.rohan.braintree.spring.service.impl.MongoUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.logging.Level;
import java.util.logging.Logger;

@RestController()
public class BraintreeController {


    private Logger logger = Logger.getLogger(BraintreeController.class.getSimpleName());

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private UserService userService;

    @PostMapping(value = "/api/payment/token" )
    public @ResponseBody
    ResponseEnvelope<String> getToken(@RequestHeader(name = "id") String userId){


        logger.log(Level.INFO,"fetching customerId for userId = " + userId);
        String customerId = userService.getCustomerId(userId);
        if (customerId!=null){
            logger.log(Level.INFO,"generating token for customerId = " + customerId);
            String token = paymentService.clientToken(customerId);
            if (token!=null) {
                return new ResponseEnvelope<>(HttpStatus.CREATED, "successfully generated client token", token);
            }else {
                return new ResponseEnvelope<>(HttpStatus.OK,"userId is not valid");
            }
        }else {
            return new ResponseEnvelope<>(HttpStatus.OK,"userId is not valid");
        }

    }



    @PostMapping(value = "/api/payment/nonce" )
    public @ResponseBody
    ResponseEnvelope<String> processPayment(@RequestParam(name = "nonce") String nonce,
                                            @RequestParam(name = "amount") double amount){


        logger.log(Level.INFO,"Received nonce is " + nonce);

        if (nonce!=null){
            boolean status = paymentService.processPayment(nonce,amount);
            if (status){
                logger.log(Level.INFO,"Payment successfully accepted by Gateway");
                return new ResponseEnvelope<>(HttpStatus.OK,"Payment successfully accepted by Gateway");
            }else {
                logger.log(Level.WARNING,"Payment rejected by Gateway");
                return new ResponseEnvelope<>(HttpStatus.OK,"Payment rejected by Gateway");
            }


        }else {
            return new ResponseEnvelope<>(HttpStatus.OK,"nonce is not valid");
        }

    }





}
