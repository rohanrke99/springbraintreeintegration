package com.rohan.braintree.spring.service.impl;

import com.braintreegateway.*;
import com.rohan.braintree.spring.BraintreeSpringIntegrationApplication;
import com.rohan.braintree.spring.service.PaymentService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class PaymentServiceImpl implements PaymentService {

    private final Logger logger = Logger.getLogger(PaymentServiceImpl.class.getSimpleName());

    @Override
    public String clientToken(String aCustomerId) {

        ClientTokenRequest clientTokenRequest = new ClientTokenRequest()
                .customerId(aCustomerId);
        return BraintreeSpringIntegrationApplication.gateway.clientToken().generate(clientTokenRequest);
    }

    @Override
    public boolean processPayment(String nonce,double amount) {
        logger.log(Level.INFO,"Submitting payment");
        TransactionRequest request = new TransactionRequest()
                .amount(new BigDecimal(amount))
                .paymentMethodNonce(nonce)
                .options()
                .submitForSettlement(true)
                .done();

        Result<Transaction> result = BraintreeSpringIntegrationApplication.gateway.transaction().sale(request);

        logger.log(Level.INFO,result.getMessage());
        if (!result.isSuccess()){
            for (ValidationError error : result.getErrors().getAllValidationErrors()){

                logger.log(Level.SEVERE,"attribute is " + error.getAttribute());
                logger.log(Level.SEVERE,"error is " + error.getMessage());
                System.out.println(" ");
            }
            return false;
        }

        return true;
    }
}
