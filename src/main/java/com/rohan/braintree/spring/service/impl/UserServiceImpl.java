package com.rohan.braintree.spring.service.impl;

import com.rohan.braintree.spring.entity.User;
import com.rohan.braintree.spring.model.LoginInModel;
import com.rohan.braintree.spring.model.UserModel;
import com.rohan.braintree.spring.repo.UserRepository;
import com.rohan.braintree.spring.rest.ResourceNotFoundException;
import com.rohan.braintree.spring.service.CustomerService;
import com.rohan.braintree.spring.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {


    @Autowired
    private UserRepository userRepo;

    @Autowired
    private CustomerService customerService;

    @Override
    public String getCustomerId(String userId) {

        User user =  userRepo.findUserById(userId);
        if (user!=null){
            return user.getCustomerId();
        }
        return null;
    }

    @Override
    public User saveUser(UserModel model) {
        User user = new User();
        user.setEmail(model.getEmail());
        user.setFirstName(model.getFirstName());
        user.setLastName(model.getLastName());
        user.setPassword(model.getPassword());

        String id = customerService.createCustomer(model);
        if (id != null){
            user.setCustomerId(id);
        }

        // Here we can save our object in any database
        //  userRepo.save(user);
        return userRepo.save(user);
    }

    @Override
    public List<User> getAllUsers() {


        return userRepo.findAll();
    }

    @Override
    public Optional<User> getUser(Long id) {
        return Optional.empty();
    }

    @Override
    public Optional<User> getUser(String email) {
        return Optional.empty();
    }


    @Override
    public User loginIn(LoginInModel model) throws IllegalAccessException,ResourceNotFoundException {

        User user = userRepo.findUserByEmail(model.getEmail());
        if (user!=null){
            if (user.getPassword().equals(model.getPassword())){
                 return user;
            }else {

                throw new IllegalAccessException("Email or password is not valid");
            }
        }
        throw new ResourceNotFoundException("No user with this email id ");
    }
}
