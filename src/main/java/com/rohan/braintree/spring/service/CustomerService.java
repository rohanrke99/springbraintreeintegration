package com.rohan.braintree.spring.service;

import com.rohan.braintree.spring.model.UserModel;

public interface CustomerService {

    String createCustomer(UserModel userModel);
}
