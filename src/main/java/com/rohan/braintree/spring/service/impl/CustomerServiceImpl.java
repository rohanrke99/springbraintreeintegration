package com.rohan.braintree.spring.service.impl;

import com.braintreegateway.Customer;
import com.braintreegateway.CustomerRequest;
import com.braintreegateway.Result;
import com.braintreegateway.ValidationError;
import com.rohan.braintree.spring.BraintreeGatewayFactory;
import com.rohan.braintree.spring.BraintreeSpringIntegrationApplication;
import com.rohan.braintree.spring.model.UserModel;
import com.rohan.braintree.spring.service.CustomerService;
import org.springframework.stereotype.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class CustomerServiceImpl implements CustomerService {


    private final Logger logger = Logger.getLogger(CustomerServiceImpl.class.getName());

    @Override
    public String createCustomer(UserModel userModel) {

        logger.log(Level.INFO,"creating customer on braintree server");

        CustomerRequest request = new CustomerRequest()
                .firstName(userModel.getFirstName())
                .lastName(userModel.getLastName())
               // .company("Jones Co.")
                .email(userModel.getEmail());
//                .fax("419-555-1234")
//                .phone("614-555-1234")
//                .website("http://example.com");
        Result<Customer> result = BraintreeSpringIntegrationApplication.gateway.customer().create(request);
         logger.log(Level.INFO,result.getMessage());
        if (result.isSuccess()){
            logger.log(Level.INFO,"success from braintree server");
            return result.getTarget().getId();
        }else {
            if (result.getErrors()!=null) {
                for (ValidationError error : result.getErrors().getAllDeepValidationErrors()) {
                    logger.log(Level.INFO,error.getMessage());
                }
            }

        }
        return null;
    }



}
