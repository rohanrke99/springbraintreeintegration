package com.rohan.braintree.spring.service;

import com.rohan.braintree.spring.entity.User;
import com.rohan.braintree.spring.model.LoginInModel;
import com.rohan.braintree.spring.model.UserModel;
import com.rohan.braintree.spring.rest.ResourceNotFoundException;

import java.util.List;
import java.util.Optional;

public interface UserService {


    User saveUser(UserModel college);

    List<User> getAllUsers();

    Optional<User> getUser(Long id);

    Optional<User> getUser(String email);


    String getCustomerId(String userId);

    User loginIn(LoginInModel model) throws IllegalAccessException,ResourceNotFoundException;

}
