package com.rohan.braintree.spring.service;

public interface PaymentService {

    String clientToken(String aCustomerId);


    boolean processPayment(String nonce,double amount);



}
