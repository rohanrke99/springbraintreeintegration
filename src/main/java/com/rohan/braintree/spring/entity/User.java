package com.rohan.braintree.spring.entity;

import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;




@Data
@ToString
@Document(collection = "user")
public class User {

    @Id
    private String id;
    @Field(value = "first_name")
    private String firstName;
    @Field(value = "last_name")
    private String lastName;

    @Field(value = "customer_id")
    @Indexed(unique = true)
    private String customerId;

    private String password;


    private String email;
}
