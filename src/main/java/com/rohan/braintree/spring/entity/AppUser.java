package com.rohan.braintree.spring.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

@Data
@ToString
@Document(collection = "app_user")
@NoArgsConstructor
@AllArgsConstructor
public class AppUser{

    @Id
    public ObjectId _id;

    public String username;
    public String password;



}
